import React, { Component } from "react";
import SlideShow from "react-image-show";

class Product extends React.Component {
  state = {
    productImages: [],
    productUrl: []
  };

  componentDidMount() {
    fetch("https://anatta-demo-app.herokuapp.com/api/products/7/image")
      .then(response => response.json())
      .then(images => this.setState({ productImages: images }));
  }

  render() {
    const { productUrl, productImages } = this.state;
    if (productImages.length > 0) {
      productImages.map((e, i) => {
        productUrl.push(e.url);
        console.log(productUrl);
      });
    }
    return (
      <SlideShow
        images={productUrl.length > 0 ? productUrl : []}
        width="920px"
        imagesWidth="450px"
        imagesHeight="300px"
        imagesHeightMobile="56vw"
        thumbnailsWidth="500px"
        thumbnailsHeight="12vw"
        indicators
        thumbnails
        fixedImagesHeight
      />
    );
  }
}

export default Product;
